import { NgModule } from '@angular/core';
import {MatToolbarModule, MatButtonModule, MatCheckboxModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MusicListModule} from './components/musiclist/musiclist.module';
import { AppComponent } from './app.component';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatToolbarModule,
    MatButtonModule,
    MatCheckboxModule,
    MusicListModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
