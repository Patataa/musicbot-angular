import { NgModule } from '@angular/core';
import {MatIconModule, MatToolbarModule, MatListModule,  MatButtonModule, MatSidenavModule, MatSliderModule} from '@angular/material';
import {MatCardModule} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MusicListComponent } from './musiclist.component';

import {MusicManagerModule} from '../../services/musicmanager/musicmanager.module';

@NgModule({
  declarations: [
    MusicListComponent
  ],
  imports: [
    BrowserAnimationsModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MusicManagerModule,
    MatIconModule,
    MatCardModule,
    MatSliderModule
  ],
  providers: [],
  exports: [MusicListComponent]
})
export class MusicListModule { }
