import { Component, OnInit } from '@angular/core';
import {MusicManagerService} from '../../services/musicmanager/musicmanager.service';

import {Observable} from 'rxjs';

interface Music {
  id: string;
  index: string;
  title: string;
}

@Component({
  selector: 'app-musiclist',
  templateUrl: './musiclist.component.html',
  styleUrls: ['./musiclist.component.css']
})
export class MusicListComponent implements OnInit {

  playlists: Array<string>;
  musics: Array<Music>;
  current: {title: string, id: string};
  progress: {current: number, total: number};
  paused: boolean;
  volume: number;
  currentPlaylist: Array<{id:string, title: string}>;
  constructor(public musicservice: MusicManagerService) {
    this.playlists = [];
    this.musics = [];
    this.current = {
      title: '',
      id: ''
    };

    this.currentPlaylist = [];
    this.volume = 0;

    this.progress = {
      current: 0,
      total: 0
    };

    this.musicservice.getPlaylists().subscribe((playlists)=> {
      this.playlists = playlists;
      console.log(this.playlists);
    });

    this.musicservice.getMusics().subscribe((musics: Array<Music>) => {

      this.musics = musics;
      console.log("Music", this.musics);
    });

    this.musicservice.current.subscribe((current) => {
      this.current = current;
    });

    this.musicservice.progress.subscribe((progress) => {
      this.progress = progress;
    });

    this.musicservice.pause.subscribe((paused) => {
      this.paused = paused;
    });

    this.musicservice.volume.subscribe((volume)=> {
      this.volume = volume;
    });

    this.musicservice.getCurrentPlaylist().subscribe((pl) => {
      this.currentPlaylist = pl;
    });
   }

  ngOnInit() {
  }

  moveMusic(event) {
    console.log(event);
    this.musicservice.moveMusic(event.value - this.progress.current);
  }

  setVolume(event) {
    this.musicservice.setVolume(event.value - this.volume);
  }

  togglePause() {
    console.log("TogglePause");
    this.musicservice.togglePause();
  }

  next() {
    this.musicservice.next();
  }

}
