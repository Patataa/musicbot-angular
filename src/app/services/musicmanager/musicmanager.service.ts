import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import 'rxjs/add/operator/map';
import {fromEvent} from 'rxjs/observable/fromEvent';
import { Socket } from 'ng-socket-io';
import {Subject} from 'rxjs/Subject';


@Injectable()
export class MusicManagerService {

    protected _progress: Subject<any> = new Subject<any>();
    protected _volume: Subject<any> = new Subject<any>();
    protected _playlists: Subject<any> = new Subject<any>();
    protected _musics: Subject<any> = new Subject<any>();
    protected _pause: Subject<any> = new Subject<any>();
    protected _current: Subject<any> = new Subject<any>();
    protected _currentPlaylist: Subject<any> = new Subject<any>();

    constructor(protected http: HttpClient, private socket: Socket) {
      this.socket
        .fromEvent('server-event')
        .subscribe(data => {
          const tmp = typeof(data);

          this.dispatchEvent(data);
        });


      this.refreshPlaylists();
      this.refreshMusics();
      this.refreshCurrent();
      this.refreshVolume();
      this.refreshCurrentPlaylist();
    }


    private getServerPath(): string {
      return 'http://localhost:80/';
    }

    dispatchEvent(data) {

      if(data['event'] !== -2) {
        console.log(data['value'])
      }
      switch (data['event']) {
        case -7:
          this._currentPlaylist.next(data['value']);
          console.log(data['value']);
          break;
        case -3:
          this._current.next(data['value']);
          console.log('current', data['value']);
          break;
        case -2:
          this._progress.next(data['value']);
          break;
        case -1:
          this._musics.next(data['value']);
          break;
        case 0:
          this.refreshMusics();
          break;
        case 2:
          this._current.next(data['value']);
          this.refreshMusics();
          break;
        case 3:
          this._volume.next(data['value']);
          break;
        case 4:
          this._pause.next(data['value']);
          this.refreshCurrent();
          break;
        case 11:
          this._currentPlaylist.next(data['value']);
          break;
        case 12:
          this._playlists.next(data['value']);
          this.refreshCurrent();
          break;

      }
    }

    setVolume(value: number) {
      this.socket.emit('cmd', 'volume ' + value);
    }

    random() {
      this.socket.emit('cmd', 'random');
      this.refreshMusics();
      this.refreshCurrent();
    }

    next() {
      this.socket.emit('cmd', 'next');
      this.refreshMusics();
      this.refreshCurrent();
    }

    moveMusic(value: number) {
      this.socket.emit('cmd', 'move ' + value);
    }

    clear() {
      this.socket.emit('cmd', 'clear');
    }

    togglePause() {
      this.socket.emit('cmd', 'pause');
    }

    refreshCurrentPlaylist() {
      this.socket.emit('cmd', 'plcur');
    }

    refreshPlaylists() {
      console.log('Refresh PL');
      this.socket.emit('cmd', 'pllist');
    }

    refreshMusics() {
      //this._musics.next([]);
      this.socket.emit('cmd', 'list');
    }

    goToPlaylist(name: string) {
      this.socket.emit('cmd', 'pl ' + name);
      this.socket.emit('cmd', 'search');
    }


    removeMusic(id: string) {
      this.socket.emit('cmd', 'rm ' + id);
      this.refreshMusics();
      this.refreshCurrent();
    }

    refreshCurrent() {
      this.socket.emit('cmd', 'current');
    }

    refreshVolume() {
      this.socket.emit('cmd', 'volume ');
    }

  public get progress(): Observable<any> {
    return this._progress.asObservable();
  }


  public get pause(): Observable<any> {
      return this._pause.asObservable();
  }

  public get current(): Observable<any> {
      return this._current.asObservable();
  }

  public get volume(): Observable<any> {
      return this._volume.asObservable();
  }

  public getCurrentPlaylist(): Observable<any> {

      return this._currentPlaylist.asObservable();
  }

  getPlaylists(): Observable<string[]> {
          return this._playlists;
    }

    getMusics(): Observable<any> {
        return this._musics;
    }


}
